package main

import (
	robots "ant/internal/robot"
	"ant/internal/session"
	"ant/internal/user/service"
	"encoding/json"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/go-chi/chi"
	"github.com/gorilla/websocket"
	"go.uber.org/zap"
)

// Handler struct
type Handler struct {
	logger         *zap.SugaredLogger
	serv           service.Service
	sessionStorage session.SessionsStorage
	tmpl           *template.Template
	connections    SocketStorage
	upgrader       websocket.Upgrader
}

// NewHandler create new handler
func NewHandler(logger *zap.Logger, srv service.Service, sessionStorage session.SessionsStorage, conns SocketStorage) *Handler {
	tmpl, err := template.New("tmpl.html").ParseFiles("html/tmpl.html")
	if err != nil {
		log.Fatalf("can't create template! %s", err)
	}

	return &Handler{
		logger:         logger.Sugar(),
		serv:           srv,
		sessionStorage: sessionStorage,
		tmpl:           tmpl,
		connections:    conns,
		upgrader: websocket.Upgrader{
			ReadBufferSize:  1024, // nolint:gomnd // buffer size to read
			WriteBufferSize: 1024, // nolint:gomnd // buffer size to write
			CheckOrigin:     func(r *http.Request) bool { return true },
		},
	}
}

// Routes adds routes to handler
func (h Handler) Routes() chi.Router {
	r := chi.NewRouter()
	r.Route("/", func(r chi.Router) {
		r.Post("/api/v1/signup", h.SignUp)
		r.Post("/api/v1/signin", h.SignIn)
		r.Put("/api/v1/users/{id}", h.Update)
		r.Get("/api/v1/users/{id}", h.GetUser)
		r.Post("/api/v1/robot", h.NewRobot)
		r.Get("/api/v1/users/{id}/robots", h.GetUsersRobots)
		r.Get("/api/v1/robots", h.GetRobots)
		r.Delete("/api/v1/robot/{id}", h.DeleteRobot)
		r.Put("/api/v1/robot/{id}/favourite", h.AddFavourite) // nolint:misspell
		r.Put("/api/v1/robot/{id}/activate", h.Activate)
		r.Put("/api/v1/robot/{id}/deactivate", h.Deactivate)
		r.Get("/api/v1/robot/{id}", h.GetRobot)
		r.Get("/ws", h.wsEndPoint)
	})

	return r
}

func (h Handler) getBody(r *http.Request) []byte {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		h.logger.Info("cannot read body!")
	}

	return body
}

// SignUp POST handler
func (h Handler) SignUp(w http.ResponseWriter, r *http.Request) {
	reqBody := h.getBody(r)
	if reqBody == nil {
		return
	}

	err := h.serv.SignUpLogic(reqBody)

	w.Header().Add("Content-type", "application/json")

	if err != nil {
		h.sendErrorJSON(w, err.Error(), http.StatusConflict)

		return
	}

	w.WriteHeader(http.StatusCreated)
}

// SignIn POST handler
func (h Handler) SignIn(w http.ResponseWriter, r *http.Request) {
	reqBody := h.getBody(r)
	if reqBody == nil {
		return
	}

	id, err := h.serv.SignInLogic(reqBody)

	w.Header().Add("Content-type", "application/json")

	if err != nil {
		h.sendErrorJSON(w, err.Error())

		return
	}

	w.WriteHeader(http.StatusOK)

	token := h.sessionStorage.CreateSession(id)
	_, err = w.Write([]byte("{\"token\": \"" + token + "\"}"))

	if err != nil {
		h.logger.Errorf("can't write into response writer:", err.Error())
	}
}

func (h Handler) sendErrorJSON(w http.ResponseWriter, errmsg string, httpcode ...int) {
	var code int
	if len(httpcode) == 0 { //nolint:gomnd // check size
		code = http.StatusBadRequest
	} else {
		code = httpcode[0] // nolint:gomnd // only first code is considers
	}

	resp := sendError{errmsg}
	data, err := json.Marshal(resp)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(code)

	_, err = w.Write(data)
	if err != nil {
		h.logger.Errorf("can't write into response writer: ", err.Error())
	}
}

func (h Handler) checkToken(w http.ResponseWriter, r *http.Request) (bool, int) {
	token := r.Header.Get("Authorization")
	is, id, err := h.sessionStorage.CheckValid(token)

	if !is {
		w.WriteHeader(http.StatusBadRequest)
		h.sendErrorJSON(w, err.Error())
	}

	return is, id
}

func (h Handler) getURLParam(r *http.Request, name string) string { // nolint: unparam
	return chi.URLParam(r, name)
}

func (h Handler) convertStrToInt(w http.ResponseWriter, str string) (int, error) {
	res, err := strconv.Atoi(str)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		h.logger.Infof("can't convert string to int: %s", err.Error)

		return 0, err
	}

	return res, nil
}

func (h Handler) compareIDs(w http.ResponseWriter, id1, id2 int) bool {
	if id1 != id2 {
		w.WriteHeader(http.StatusForbidden)
		h.sendErrorJSON(w, "access denied")

		return false
	}

	return true
}

// Update PUT handler
func (h Handler) Update(w http.ResponseWriter, r *http.Request) {
	is, id := h.checkToken(w, r)
	if !is {
		return
	}

	reqBody := h.getBody(r)
	if reqBody == nil {
		return
	}

	userID, err := h.convertStrToInt(w, h.getURLParam(r, "id"))
	if err != nil {
		return
	}

	if !h.compareIDs(w, id, userID) {
		return
	}

	u, err := h.serv.UpdateUser(reqBody, id)
	if err != nil {
		http.NotFound(w, r)
		h.sendErrorJSON(w, err.Error())

		return
	}

	entity := userEntity{u.Name, u.Email, u.Birthday}
	err = json.NewEncoder(w).Encode(entity)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
}

// GetUser GET handler
func (h Handler) GetUser(w http.ResponseWriter, r *http.Request) {
	is, id := h.checkToken(w, r)
	if !is {
		return
	}

	userID, err := h.convertStrToInt(w, h.getURLParam(r, "id"))
	if err != nil {
		return
	}

	if !h.compareIDs(w, id, userID) {
		return
	}

	u, err := h.serv.SearchUser(id)
	if err != nil {
		h.logger.Errorf(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
	}

	entity := userEntity{u.Name, u.Email, u.Birthday}
	err = json.NewEncoder(w).Encode(entity)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
}

// NewRobot POST handler
func (h Handler) NewRobot(w http.ResponseWriter, r *http.Request) {
	is, id := h.checkToken(w, r)
	if !is {
		return
	}

	reqBody := h.getBody(r)
	if reqBody == nil {
		return
	}

	err := h.serv.NewRobot(reqBody, id)
	if err != nil {
		h.sendErrorJSON(w, err.Error())
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func (h Handler) sendResult(w http.ResponseWriter, acceptType string, robots []robots.Robot) {
	switch acceptType {
	case "application/json":
		var err error

		var data []byte

		if len(robots) == 1 { // nolint:gomnd // check size
			data, err = json.MarshalIndent(robots[0], "", "\t") // nolint:gomnd // set view of json writer
		} else {
			data, err = json.MarshalIndent(robots, "", "\t")
		}

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			h.logger.Info("can't encode into json: %s", err)

			return
		}

		w.WriteHeader(http.StatusOK)

		_, err = w.Write(data)
		if err != nil {
			h.logger.Errorf("can't write into response writer: ", err.Error())
		}
	default:
		if strings.Contains(acceptType, "text/html") { // else doing nothing
			err := h.tmpl.ExecuteTemplate(w, "tmpl.html", robots)
			if err != nil {
				h.logger.Info("can't execute template! %s", err)
			}
		}
	}
}

// GetRobots GET handler
func (h Handler) GetRobots(w http.ResponseWriter, r *http.Request) {
	is, _ := h.checkToken(w, r)
	if !is {
		return
	}

	ticker := r.URL.Query().Get("ticker")
	userStr := r.URL.Query().Get("user")

	var userID int

	if userStr != "" {
		uID, err := h.convertStrToInt(w, r.URL.Query().Get("user"))
		if err != nil {
			return
		}

		userID = uID
	} else {
		userID = -1
	}

	acceptType := r.Header.Get("accept")

	robots, err := h.serv.RobotsGet(ticker, userID)
	if err != nil {
		h.sendErrorJSON(w, err.Error())
		return
	}

	h.sendResult(w, acceptType, robots)
}

// GetUsersRobots GET handler
func (h Handler) GetUsersRobots(w http.ResponseWriter, r *http.Request) {
	is, id := h.checkToken(w, r)
	if !is {
		return
	}

	acceptType := r.Header.Get("accept")

	userID, err := h.convertStrToInt(w, h.getURLParam(r, "id"))
	if err != nil {
		return
	}

	if !h.compareIDs(w, id, userID) {
		return
	}

	robots, err := h.serv.RobotsGetByID(id)
	if err != nil {
		h.sendErrorJSON(w, "robot was not found")
		return
	}

	h.sendResult(w, acceptType, robots)
}

// DeleteRobot DELETE handler
func (h Handler) DeleteRobot(w http.ResponseWriter, r *http.Request) {
	is, id := h.checkToken(w, r)
	if !is {
		return
	}

	robotID, err := h.convertStrToInt(w, h.getURLParam(r, "id"))
	if err != nil {
		return
	}

	err = h.serv.DeleteRobot(id, robotID)
	if err != nil {
		h.sendErrorJSON(w, err.Error())
		return
	}

	w.WriteHeader(http.StatusOK)
}

// AddFavourite PUT handler
func (h Handler) AddFavourite(w http.ResponseWriter, r *http.Request) {
	is, id := h.checkToken(w, r)
	if !is {
		return
	}

	robotID, err := h.convertStrToInt(w, h.getURLParam(r, "id"))
	if err != nil {
		return
	}

	err = h.serv.AddFavouriteRobot(id, robotID)
	if err != nil {
		h.sendErrorJSON(w, err.Error())
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (h Handler) changeActivity(w http.ResponseWriter, r *http.Request, activate bool) {
	is, id := h.checkToken(w, r)
	if !is {
		return
	}

	robotID, err := h.convertStrToInt(w, h.getURLParam(r, "id"))
	if err != nil {
		return
	}

	var robot *robots.Robot

	if activate {
		robot, err = h.serv.ActivateRobot(id, robotID)
	} else {
		robot, err = h.serv.DeactivateRobot(id, robotID)
	}

	if err != nil {
		h.sendErrorJSON(w, err.Error())
		return
	}

	err = h.Broadcast(*robot)
	if err != nil {
		h.logger.Info(err.Error())
	}

	w.WriteHeader(http.StatusOK)
}

// Activate PUT handler
func (h Handler) Activate(w http.ResponseWriter, r *http.Request) {
	h.changeActivity(w, r, true)
}

// Deactivate PUT handler
func (h Handler) Deactivate(w http.ResponseWriter, r *http.Request) {
	h.changeActivity(w, r, false)
}

// GetRobot GET handler
func (h Handler) GetRobot(w http.ResponseWriter, r *http.Request) {
	is, id := h.checkToken(w, r)
	if !is {
		return
	}

	robotID, err := h.convertStrToInt(w, h.getURLParam(r, "id"))
	if err != nil {
		return
	}

	robot, err := h.serv.GetRobot(id, robotID)
	if err != nil {
		h.sendErrorJSON(w, "robot was not found")
	}

	acceptType := r.Header.Get("accept")

	var bots []robots.Robot
	bots = append(bots, *robot)

	h.sendResult(w, acceptType, bots)
}

func (h Handler) wsEndPoint(w http.ResponseWriter, r *http.Request) {
	conn, err := h.upgrader.Upgrade(w, r, nil)
	if err != nil {
		h.logger.Info("can't upgrade connection: %s", err)
		conn.Close()
	}

	_, p, err := conn.ReadMessage()
	if err != nil {
		h.logger.Info("can't get client's message: %s", err)
	}

	msg := string(p)
	h.connections.add(newConnection(conn, msg))
}

// Broadcast function give changes to websocket connections
func (h Handler) Broadcast(bot robots.Robot) error {
	return h.connections.sendForAll(bot)
}

package main

import (
	fintech "ant/internal/streamer"
	"ant/internal/user/postgres"
	"ant/internal/user/service"
	"context"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"go.uber.org/zap"
	"google.golang.org/grpc"
)

type sendError struct {
	SErr string `json:"error"`
}

type userEntity struct {
	Name     string     `json:"name"`
	Email    string     `json:"email"`
	Birthday *time.Time `json:"birthday"`
}

func setConfig(h http.Handler) *http.Server {
	addr := net.JoinHostPort("", "5000")

	return &http.Server{Addr: addr, Handler: h}
}

func main() {
	logger, err := zap.NewDevelopment()
	if err != nil {
		log.Fatal("Can't create zap logger: ", err)
	}

	db, err := postgres.SQLConnect(logger)
	if err != nil {
		logger.Sugar().Fatal("Can't create db: ", err)
	}

	if err = db.CheckConnection(); err != nil {
		logger.Sugar().Fatalf("Can't check connection: %s", err)
	}

	defer db.SQLClose()

	userStorage, err := postgres.CreateUserStorage(db)
	if err != nil {
		logger.Sugar().Fatal("Can't create user storage: ", err)
	}

	defer handleCloser(logger, "userStorage", userStorage)

	robotStorage, err := postgres.CreateRobotStorage(db)
	if err != nil {
		logger.Sugar().Fatal("Can't create robot's storage: ", err)
	}

	defer handleCloser(logger, "robotStorage", robotStorage)

	serv := service.NewService(userStorage, robotStorage)

	sessionStorage, err := postgres.CreateNewSessionStorage(db)
	if err != nil {
		logger.Sugar().Fatal("Can't create session storage: ", err)
	}

	defer handleCloser(logger, "sessionStorage", sessionStorage)

	connections := newStorage(logger)
	defer connections.closeAll()

	h := NewHandler(logger, *serv, sessionStorage, connections)

	r := h.Routes()

	s := grpc.NewServer()
	fintech.RegisterTradingServiceServer(s, &fintech.UnimplementedTradingServiceServer{})

	startServer(logger, r)
}

func startServer(logger *zap.Logger, r http.Handler) {
	stopAppCh := make(chan struct{})
	sigquit := make(chan os.Signal, 1)

	signal.Ignore(syscall.SIGHUP, syscall.SIGPIPE)

	signal.Notify(sigquit, syscall.SIGINT, syscall.SIGTERM)

	srv := setConfig(r)

	go func() {
		<-sigquit

		if err := srv.Shutdown(context.Background()); err != nil {
			logger.Sugar().Fatalf("could not shutdown server: %s", err)
		}
		stopAppCh <- struct{}{}
	}()

	if err := srv.ListenAndServe(); err != nil {
		logger.Sugar().Fatalf("can't listen and serve server: %s", err)
	}
}

func handleCloser(logger *zap.Logger, resource string, closer io.Closer) {
	if err := closer.Close(); err != nil {
		logger.Sugar().Errorf("Can't close %q: %s", resource, err)
	}
}

package main

import (
	robots "ant/internal/robot"
	"fmt"

	"github.com/gorilla/websocket"
	"go.uber.org/zap"
)

// SocketConnection struct to broadcast changes to clients
type SocketConnection struct {
	ws  *websocket.Conn
	url string
	id  int
}

func newConnection(conn *websocket.Conn, path string) *SocketConnection {
	return &SocketConnection{
		ws:  conn,
		url: path,
	}
}

// SocketStorage struct to contain socket connections
type SocketStorage struct {
	sc     []*SocketConnection
	logger *zap.SugaredLogger
	connID int
}

func newStorage(logger *zap.Logger) SocketStorage {
	var sc []*SocketConnection

	return SocketStorage{
		sc:     sc,
		logger: logger.Sugar(),
		connID: 0,
	}
}

func (ss *SocketStorage) add(sc *SocketConnection) {
	out := make(chan bool)
	sc.id = ss.connID
	ss.connID++
	ss.sc = append(ss.sc, sc)

	listen := func() {
		for {
			_, _, err := sc.ws.ReadMessage()
			if err != nil {
				out <- true
				return
			}
		}
	}

	go listen()

	waitForEnd := func(id int) {
		for range out {
			ss.close(id)
			return
		}
	}

	go waitForEnd(sc.id)
}

func (ss *SocketStorage) sendForAll(robot robots.Robot) error {
	for _, conn := range ss.sc {
		err := conn.ws.WriteJSON(robot)
		if err != nil {
			return fmt.Errorf("can't send message to client: %s", err.Error())
		}
	}

	return nil
}

func (ss *SocketStorage) close(id int) {
	for num, c := range ss.sc {
		if c.id == id {
			_ = ss.sc[num].ws.Close()
			ss.sc = append(ss.sc[:num], ss.sc[num+1:]...)

			break
		}
	}
}

func (ss *SocketStorage) closeAll() {
	for _, conn := range ss.sc {
		err := conn.ws.Close()
		if err != nil {
			ss.logger.Info("can't close websocket connection: %s", err.Error())
		}

		ss.sc = ss.sc[0:]
	}
}

module ant

go 1.13

require (
	github.com/go-chi/chi v4.1.1+incompatible
	github.com/golang/protobuf v1.4.1
	github.com/gorilla/websocket v1.4.2
	github.com/lib/pq v1.5.0
	github.com/pkg/errors v0.9.1
	go.uber.org/zap v1.15.0
	golang.org/x/crypto v0.0.0-20200429183012-4b2356b1ed79
	golang.org/x/tools v0.0.0-20200513122804-866d71a3170a // indirect
	google.golang.org/grpc v1.29.1
	google.golang.org/protobuf v1.22.0
)

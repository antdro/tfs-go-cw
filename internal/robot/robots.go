package robots

import "time"

// Robot structure
type Robot struct {
	RobotID       int        `json:"robot_id"`
	OwnerUserID   int        `json:"owner_user_id"`
	ParentRobotID int        `json:"parent_robot_id"`
	IsFavourite   bool       `json:"is_favourite"` // nolint:misspell
	IsActive      bool       `json:"is_active"`
	Ticker        string     `json:"ticker"`
	BuyPrice      float64    `json:"buy_price"`
	SellPrice     float64    `json:"sell_price"`
	PlanStart     *time.Time `json:"plan_start"`
	PlanEnd       *time.Time `json:"plan_end"`
	PlanYield     float64    `json:"plan_yield"`
	FactYield     float64    `json:"fact_yield"`
	DealsCount    int        `json:"deals_count"`
	ActivatedAt   *time.Time `json:"activated_at"`
	DeactivatedAt *time.Time `json:"deactivated_at"`
	CreatedAt     *time.Time `json:"created_at"`
	DeletedAt     *time.Time `json:"deleted_at"`
}

// Storage functions to work with robots
type Storage interface {
	CreateRobot(r *Robot) error
	GetRobots(string, int) ([]Robot, error)
	GetRobotsByID(int) ([]Robot, error)
	DeleteRobot(int, int) error
	FindOneRow(int, int) (*Robot, error)
	AddRobotToFavourite(int, int) error
	ActivateRobot(int, int) (*Robot, error)
	DeactivateRobot(int, int) (*Robot, error)
	GetRobot(int, int) (*Robot, error)
}

package session

import (
	"time"
)

// Session structure
type Session struct {
	SessionID  string
	UserID     int
	CreatedAt  *time.Time
	ValidUntil *time.Time // 30 min after created
}

// SessionsStorage interface for functions working with DB
type SessionsStorage interface {
	FindSession(string) (*Session, error)
	AddSession(Session) error
	CreateSession(int) string
	CheckValid(string) (bool, int, error)
}

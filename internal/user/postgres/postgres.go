package postgres

import (
	"database/sql"
	"fmt"

	"go.uber.org/zap"

	_ "github.com/lib/pq" // postgres driver
	"github.com/pkg/errors"
)

const (
	host     = "localhost"
	port     = 5432
	dbuser   = "postgres"
	password = "123"
	dbname   = "tfs"
)

// Dbsql struct for connection
type Dbsql struct {
	DB     *sql.DB
	Logger *zap.Logger
	Open   bool
}

// SQLConnect function to open connection
func SQLConnect(log *zap.Logger) (*Dbsql, error) {
	var d Dbsql

	dsn := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, dbuser, password, dbname)

	db, err := sql.Open("postgres", dsn)

	if err != nil {
		return nil, errors.Wrap(err, "can't connect to db: %s")
	}

	d.DB = db
	d.Logger = log
	d.Open = true

	return &d, nil
}

// CheckConnection function trying to ping db
func (d *Dbsql) CheckConnection() error {
	if err := d.DB.Ping(); err != nil {
		return errors.Wrap(err, "can't ping db: %s")
	}

	return nil
}

// SQLClose function to close connection
func (d Dbsql) SQLClose() error {
	err := d.DB.Close()
	if err != nil {
		return fmt.Errorf("cannot close db! %s", err)
	}

	d.Open = false

	return nil
}

package postgres

import (
	robots "ant/internal/robot"
	"database/sql"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/pkg/errors"
)

var _ robots.Storage = &RobotStorage{}

// RobotStorage struct implement the RobotsStorage interface
type RobotStorage struct {
	StatementStorage

	createStmt     *sql.Stmt
	findStmt       *sql.Stmt
	findOne        *sql.Stmt
	findByIDStmt   *sql.Stmt
	deleteStmt     *sql.Stmt
	verifyStmt     *sql.Stmt
	activateStmt   *sql.Stmt
	deactivateStmt *sql.Stmt
}

const columns = "owner_user_id, parent_robot_id, is_favourite, " + // nolint:misspell
	"is_active, ticker, buy_price, sell_price, plan_start, plan_end, plan_yield, " +
	"fact_yield, deals_count, activated_at, deactivated_at, created_at, deleted_at"

const createRobotQ = "INSERT INTO robots (" + columns + ") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16) RETURNING robot_id"
const getRobotsQ = "SELECT robot_id," + columns + " FROM robots"
const getRobotQ = getRobotsQ + " WHERE robot_id = $1"
const getRobotsByIDQ = getRobotsQ + " WHERE owner_user_id = $1"
const softDeleteQ = "UPDATE robots SET deleted_at = $1 WHERE robot_id = $2 RETURNING robot_id"
const verifyQ = "SELECT robot_id FROM robots WHERE owner_user_id = $1 AND robot_id = $2"
const activateQ = "UPDATE robots SET is_active = true, activated_at = $1 WHERE robot_id = $2"
const deactivateQ = "UPDATE robots SET is_active = false, deactivated_at = $1 WHERE robot_id = $2"

// CreateRobotStorage creates new user storage
func CreateRobotStorage(d *Dbsql) (*RobotStorage, error) {
	s := &RobotStorage{StatementStorage: Create(d)}

	stmts := []stmt{
		{Query: createRobotQ, Dst: &s.createStmt},
		{Query: getRobotsQ, Dst: &s.findStmt},
		{Query: getRobotsByIDQ, Dst: &s.findByIDStmt},
		{Query: softDeleteQ, Dst: &s.deleteStmt},
		{Query: verifyQ, Dst: &s.verifyStmt},
		{Query: getRobotQ, Dst: &s.findOne},
		{Query: activateQ, Dst: &s.activateStmt},
		{Query: deactivateQ, Dst: &s.deactivateStmt},
	}

	if err := s.initStatements(stmts); err != nil {
		return nil, errors.Wrap(err, "can't init statements")
	}

	return s, nil
}

// UnmarshalRobot convert json to robot
func UnmarshalRobot(data []byte) (robots.Robot, error) {
	var r robots.Robot
	err := json.Unmarshal(data, &r)

	return r, err
}

// CreateRobot create new robot
func (rs *RobotStorage) CreateRobot(r *robots.Robot) error {
	r.CreatedAt = currentTime()
	if r.PlanStart == nil {
		planstart := currentTime().Add(time.Hour * 4) // nolint:gomnd // if planstart time is unknown, function add 4 hours to creation time
		planend := planstart.Add(time.Hour * 4)       // nolint:gomnd // if planend time is unknown, function add 4 hours to start time
		r.PlanStart = &planstart
		r.PlanEnd = &planend
	}

	err := rs.createStmt.QueryRow(&r.OwnerUserID, &r.ParentRobotID, &r.IsFavourite,
		&r.IsActive, &r.Ticker, &r.BuyPrice, &r.SellPrice, &r.PlanStart, &r.PlanEnd, &r.PlanYield,
		&r.FactYield, &r.DealsCount, &r.ActivatedAt, &r.DeactivatedAt, &r.CreatedAt, &r.DeletedAt).Scan(&r.RobotID)

	if err != nil {
		return errors.Wrap(err, "can't create robot")
	}

	return nil
}

func fillSliceRobots(rows *sql.Rows) ([]robots.Robot, error) {
	var bots []robots.Robot

	for rows.Next() {
		var r robots.Robot
		err := rows.Scan(&r.RobotID, &r.OwnerUserID, &r.ParentRobotID, &r.IsFavourite,
			&r.IsActive, &r.Ticker, &r.BuyPrice, &r.SellPrice, &r.PlanStart, &r.PlanEnd, &r.PlanYield,
			&r.FactYield, &r.DealsCount, &r.ActivatedAt, &r.DeactivatedAt, &r.CreatedAt, &r.DeletedAt)

		if err != nil {
			return nil, errors.Wrap(err, "can't scan robot")
		}

		bots = append(bots, r)
	}

	return bots, nil
}

// GetRobots returns slice of robots
func (rs *RobotStorage) GetRobots(ticker string, userID int) ([]robots.Robot, error) {
	var causes string

	var rows *sql.Rows

	if ticker == "" && userID == -1 { // if no GET-query params
		rws, err := rs.findStmt.Query()

		if err != nil {
			return nil, errors.Wrap(err, "can't get query")
		}

		if rws.Err() != nil {
			return nil, errors.Wrap(err, "problem with rows")
		}

		rows = rws
	} else {
		var findcauses *sql.Stmt
		causes = causes + getRobotsQ + " WHERE"
		if ticker != "" {
			if userID != -1 {
				causes = causes + " ticker = '" + ticker + "' AND owner_user_id = " + strconv.Itoa(userID)
			} else {
				causes = causes + " ticker = '" + ticker + "'"
			}
		} else {
			causes = causes + " owner_user_id = " + strconv.Itoa(userID)
		}
		findcauses, err := rs.prepareStatement(causes)
		if err != nil {
			return nil, errors.Wrapf(err, "can't prepare statements")
		}
		rws, err := findcauses.Query()
		if err != nil {
			return nil, errors.Wrapf(err, "can't exec query")
		}

		if rws.Err() != nil {
			return nil, errors.Wrap(err, "problem with rows")
		}

		rows = rws
	}

	return fillSliceRobots(rows)
}

// GetRobotsByID returns slice of robots by id
func (rs *RobotStorage) GetRobotsByID(id int) ([]robots.Robot, error) {
	rows, err := rs.findByIDStmt.Query(id)

	if err != nil {
		return nil, errors.Wrap(err, "can't get query")
	}

	return fillSliceRobots(rows)
}

// DeleteRobot updates the field 'deleted_at' by robot id
func (rs *RobotStorage) DeleteRobot(userID int, robotID int) error {
	r, err := rs.FindOneRow(userID, robotID)
	if err != nil {
		return err
	}

	if r.DeletedAt != nil {
		return fmt.Errorf("robot is already deleted")
	}

	time := currentTime()
	err = rs.deleteStmt.QueryRow(time, robotID).Scan(&r.RobotID)

	if err != nil {
		return fmt.Errorf("can't delete robot")
	}

	return err
}

// FindOneRow finds one robot by user's and robot's id
func (rs *RobotStorage) FindOneRow(userID, robotID int) (*robots.Robot, error) {
	var r robots.Robot
	err := rs.findOne.QueryRow(robotID).Scan(&r.RobotID, &r.OwnerUserID, &r.ParentRobotID, &r.IsFavourite,
		&r.IsActive, &r.Ticker, &r.BuyPrice, &r.SellPrice, &r.PlanStart, &r.PlanEnd, &r.PlanYield,
		&r.FactYield, &r.DealsCount, &r.ActivatedAt, &r.DeactivatedAt, &r.CreatedAt, &r.DeletedAt)

	if err != nil {
		return nil, fmt.Errorf("can't scan robot: %s", err)
	}

	if userID != r.OwnerUserID {
		return nil, fmt.Errorf("robot is not exist")
	}

	return &r, nil
}

// AddRobotToFavourite create new row with field favorite = true
func (rs *RobotStorage) AddRobotToFavourite(userID, robotID int) error {
	r, err := rs.FindOneRow(userID, robotID)

	if err != nil {
		return err
	}

	r.IsFavourite = true
	r.IsActive = false
	r.FactYield = 0
	r.DealsCount = 0
	r.ParentRobotID = robotID
	r.OwnerUserID = userID
	err = rs.CreateRobot(r)

	return err
}

// ActivateRobot activates robot
func (rs *RobotStorage) ActivateRobot(userID, robotID int) (*robots.Robot, error) {
	r, err := rs.FindOneRow(userID, robotID)

	if err != nil {
		return nil, err
	}

	now := time.Now()
	if !(checkTime(now, *r.PlanStart) || checkTime(*r.PlanEnd, now)) {
		return nil, fmt.Errorf("robot can't be activated at this time")
	}

	rows, err := rs.activateStmt.Query(now, robotID)

	if err != nil {
		return nil, errors.Wrap(err, "can't activate robot")
	}

	if rows.Err() != nil {
		return nil, errors.Wrap(err, "problems with rows")
	}

	r.IsActive = true
	r.ActivatedAt = &now

	return r, nil
}

// DeactivateRobot deactivates robot
func (rs *RobotStorage) DeactivateRobot(userID, robotID int) (*robots.Robot, error) {
	r, err := rs.FindOneRow(userID, robotID)

	if err != nil {
		return nil, err
	}

	if !r.IsActive {
		return nil, fmt.Errorf("robot is already deactivated")
	}

	now := time.Now()
	if !(checkTime(now, *r.PlanStart) || checkTime(*r.PlanEnd, now)) {
		return nil, fmt.Errorf("robot can't be deactivated at this time")
	}

	rows, err := rs.deactivateStmt.Query(now, robotID)

	if err != nil {
		return nil, errors.Wrap(err, "can't deactivate robot")
	}

	if rows.Err() != nil {
		return nil, errors.Wrap(err, "problems with rows")
	}

	r.IsActive = false
	r.DeactivatedAt = &now

	return r, nil
}

// GetRobot get info about robot by id
func (rs *RobotStorage) GetRobot(userID, robotID int) (*robots.Robot, error) {
	r, err := rs.FindOneRow(userID, robotID)

	if err != nil {
		return nil, err
	}

	return r, nil
}

package postgres

import (
	"ant/internal/session"
	"database/sql"
	"fmt"
	"math/rand"
	"strings"
	"time"

	"github.com/pkg/errors"
)

var _ session.SessionsStorage = &SessionStorage{}

// SessionStorage struct for work with sessions
type SessionStorage struct {
	StatementStorage

	createStmt *sql.Stmt
	findStmt   *sql.Stmt
	updateStmt *sql.Stmt
}

const createSessionQ = "INSERT INTO sessions VALUES ($1, $2, $3, $4)"
const findSessionQ = "SELECT session_id, user_id, created_at, valid_until FROM sessions WHERE session_id = $1"
const updateSessionQ = "UPDATE sessions SET created_at = $1, valid_until = $2 WHERE session_id = $3"

// CreateNewSessionStorage create new session storage
func CreateNewSessionStorage(d *Dbsql) (*SessionStorage, error) {
	s := &SessionStorage{StatementStorage: Create(d)}

	stmts := []stmt{
		{Query: createSessionQ, Dst: &s.createStmt},
		{Query: findSessionQ, Dst: &s.findStmt},
		{Query: updateSessionQ, Dst: &s.updateStmt},
	}

	if err := s.initStatements(stmts); err != nil {
		return nil, errors.Wrap(err, "can't init statements")
	}

	return s, nil
}

// FindSession find session by token (session_id)
func (ss *SessionStorage) FindSession(token string) (*session.Session, error) {
	var s session.Session
	if err := ss.findStmt.QueryRow(token).Scan(&s.SessionID, &s.UserID, &s.CreatedAt, &s.ValidUntil); err != nil {
		return nil, fmt.Errorf("session was not found")
	}

	return &s, nil
}

// AddSession implements SessionsStorage interface
func (ss *SessionStorage) AddSession(s session.Session) error {
	_ = ss.createStmt.QueryRow(&s.SessionID, &s.UserID, &s.CreatedAt, &s.ValidUntil)

	return nil
}

// CheckValid function checks valid of token
func (ss *SessionStorage) CheckValid(str string) (bool, int, error) {
	s, err := ss.FindSession(str)
	if err != nil {
		ss.db.Logger.Sugar().Infof(err.Error())
		return false, 0, err
	}

	now := time.Now()

	ok := checkTime(now, *s.ValidUntil)

	if !ok {
		err = fmt.Errorf("validity of token has been expired")
	}

	return checkTime(now, *s.ValidUntil), s.UserID, err
}

func checkTime(cur, t time.Time) bool {
	return cur.Before(t) || cur.Equal(t)
}

func generateBearerToken() string {
	chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")

	rand.Seed(time.Now().UnixNano())

	length := 10

	var b strings.Builder

	for i := 0; i < length; i++ {
		b.WriteRune(chars[rand.Intn(len(chars))])
	}

	return b.String()
}

// CreateSession makes session for sign in user and returns token
func (ss *SessionStorage) CreateSession(userID int) string {
	token := generateBearerToken()

	createdTime := time.Now()
	valid := createdTime.Add(time.Minute * 30) // nolint:gomnd // time of session's validity

	s := session.Session{SessionID: token, UserID: userID, CreatedAt: &createdTime, ValidUntil: &valid}

	err := ss.AddSession(s)
	if err != nil {
		ss.db.Logger.Sugar().Errorf(err.Error())
		return ""
	}

	return token
}

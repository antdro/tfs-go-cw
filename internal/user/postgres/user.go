package postgres

import (
	"ant/internal/user"

	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/lib/pq"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

var _ user.Storage = &UserStorage{}

// UserStorage struct implement the Storage interface
type UserStorage struct {
	StatementStorage

	createStmt   *sql.Stmt
	findStmt     *sql.Stmt
	updateStmt   *sql.Stmt
	findByIDStmt *sql.Stmt
}

const createUserQ = "INSERT INTO users (name, email, password, birthday, created_at, updated_at) VALUES ($1, $2, $3, $4, $5, $6) RETURNING ID"
const findUserQ = "SELECT id, name, email, password, birthday, created_at, updated_at FROM users WHERE email = $1"
const updateUserQ = "UPDATE users SET name = $1, email = $2, birthday = $3, password = $4, updated_at = $5 WHERE id = $6 RETURNING name, email, birthday"
const findUserByIDQ = "SELECT name, email, birthday FROM users WHERE id = $1"

// CreateUserStorage creates new user storage
func CreateUserStorage(d *Dbsql) (*UserStorage, error) {
	s := &UserStorage{StatementStorage: Create(d)}

	stmts := []stmt{
		{Query: createUserQ, Dst: &s.createStmt},
		{Query: findUserQ, Dst: &s.findStmt},
		{Query: updateUserQ, Dst: &s.updateStmt},
		{Query: findUserByIDQ, Dst: &s.findByIDStmt},
	}

	if err := s.initStatements(stmts); err != nil {
		return nil, errors.Wrap(err, "can't init statements")
	}

	return s, nil
}

func hash(password string) (string, error) {
	pwd, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(pwd), err
}

func checkPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

// SignUpDB function is trying to add user into DB
func (us *UserStorage) SignUpDB(body []byte) error {
	u, err := UnmarshalUser(body)
	if err != nil {
		log.Fatalf("problems with unmarshalling json\n")
	}

	pwd, err := hash(u.Password)

	if err != nil {
		log.Fatalf("problems with hashing for password\n")
	}

	u.Password = pwd
	u.CreatedAt = currentTime()
	u.UpdatedAt = currentTime()
	err = us.InsertUser(&u)

	return err
}

// SignInDB function check user in DB
func (us *UserStorage) SignInDB(body []byte) (int, error) {
	u, err := UnmarshalUser(body)
	if err != nil {
		return 0, fmt.Errorf("problems with unmarshalling json")
	}

	id, hash, err := us.SearchUser(u.Email)

	if err != nil {
		return 0, fmt.Errorf(err.Error())
	}

	res := checkPasswordHash(u.Password, hash)

	if !res {
		return 0, fmt.Errorf("incorrect email or password")
	}

	return id, nil
}

// InsertUser function try to insert user into stmt db
func (us *UserStorage) InsertUser(u *user.User) error {
	err := us.createStmt.QueryRow(&u.Name, &u.Email, &u.Password, &u.Birthday, &u.CreatedAt, &u.UpdatedAt).Scan(&u.ID)
	if err, ok := err.(*pq.Error); ok {
		{
			if err.Code.Name() == "unique_violation" {
				if err.Constraint == "unique_email" {
					return fmt.Errorf("user %s is already registered", u.Email)
				}
			}
		}

		return errors.Wrap(err, "can't insert user")
	}

	return nil
}

// UpdateUserDB function updates user's name by id
func (us *UserStorage) UpdateUserDB(body []byte, id int) (*user.User, error) {
	u, err := UnmarshalUser(body)
	if err != nil {
		return nil, fmt.Errorf("problems with unmarshalling json")
	}

	return us.UpdateUser(u, id)
}

// SearchUser is searching user in storage and returns id and hash of password
func (us *UserStorage) SearchUser(email string) (int, string, error) {
	var u user.User

	row := us.findStmt.QueryRow(email)

	if err := row.Scan(&u.ID, &u.Name, &u.Email, &u.Password, &u.Birthday, &u.CreatedAt, &u.UpdatedAt); err != nil {
		return 0, "", fmt.Errorf("incorrect email or password")
	}

	return u.ID, u.Password, nil
}

// SearchUserByID is searching user in storage by id
func (us *UserStorage) SearchUserByID(id int) (*user.User, error) {
	var u user.User

	row := us.findByIDStmt.QueryRow(id)

	if err := row.Scan(&u.Name, &u.Email, &u.Birthday); err != nil {
		return nil, fmt.Errorf("incorrect email or password")
	}

	return &u, nil
}

// UpdateUser updates info about user
func (us *UserStorage) UpdateUser(usr user.User, id int) (*user.User, error) {
	if usr.Name == "" || usr.Email == "" || usr.Password == "" {
		return nil, fmt.Errorf("field must be not null")
	}

	var u user.User

	pass, err := hash(usr.Password)
	if err != nil {
		return nil, err
	}

	row := us.updateStmt.QueryRow(usr.Name, usr.Email, usr.Birthday, pass, currentTime(), id)
	if err := row.Scan(&u.Name, &u.Email, &u.Birthday); err != nil {
		return nil, fmt.Errorf("user %s is already registered", usr.Email)
	}

	return &u, nil
}

// UnmarshalUser convert json to user
func UnmarshalUser(data []byte) (user.User, error) {
	var u user.User
	err := json.Unmarshal(data, &u)

	return u, err
}

// CurrentTimeToString convert current UNIX's time to string
func CurrentTimeToString() string {
	return time.Now().Format(time.RFC3339)
}

func currentTime() *time.Time {
	t := time.Now()
	return &t
}

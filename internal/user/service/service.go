package service

import (
	robots "ant/internal/robot"
	"ant/internal/user"
	usr "ant/internal/user/postgres"
)

// Service structure
type Service struct {
	us usr.UserStorage
	rs usr.RobotStorage
}

// NewService create new service
func NewService(ustor *usr.UserStorage, rstor *usr.RobotStorage) *Service {
	return &Service{us: *ustor, rs: *rstor}
}

// SignUpLogic function is middleware between SignUp handler and DB logic
func (s Service) SignUpLogic(body []byte) error {
	return s.us.SignUpDB(body)
}

// SignInLogic function is middleware between SignIn handler and DB logic
func (s Service) SignInLogic(body []byte) (int, error) {
	return s.us.SignInDB(body)
}

// SearchUser function is middleware between getUser handler and DB logic
func (s Service) SearchUser(id int) (*user.User, error) {
	return s.us.SearchUserByID(id)
}

// UpdateUser function is middleware between Update handler and DB logic
func (s Service) UpdateUser(body []byte, id int) (*user.User, error) {
	return s.us.UpdateUserDB(body, id)
}

// NewRobot function is middleware between CreateRobot handler and DB logic
func (s Service) NewRobot(body []byte, id int) error {
	r, err := usr.UnmarshalRobot(body)
	if err != nil {
		return err
	}

	if r.OwnerUserID == 0 {
		r.OwnerUserID = id
	}

	return s.rs.CreateRobot(&r)
}

// RobotsGet function is middleware between GetRobots handler and DB logic
func (s Service) RobotsGet(ticker string, userID int) ([]robots.Robot, error) {
	return s.rs.GetRobots(ticker, userID)
}

// RobotsGetByID function is middleware between GetRobotsByID handler and DB logic
func (s Service) RobotsGetByID(id int) ([]robots.Robot, error) {
	return s.rs.GetRobotsByID(id)
}

// DeleteRobot function is middleware between DeleteRobot handler and DB logic
func (s Service) DeleteRobot(userID, robotID int) error {
	return s.rs.DeleteRobot(userID, robotID)
}

// AddFavouriteRobot function is middleware between AddFavourite handler and DB logic
func (s Service) AddFavouriteRobot(userID, robotID int) error {
	return s.rs.AddRobotToFavourite(userID, robotID)
}

// ActivateRobot function is middleware between Activate handler and DB logic
func (s Service) ActivateRobot(userID, robotID int) (*robots.Robot, error) {
	return s.rs.ActivateRobot(userID, robotID)
}

// DeactivateRobot function is middleware between Deactivate handler and DB logic
func (s Service) DeactivateRobot(userID, robotID int) (*robots.Robot, error) {
	return s.rs.DeactivateRobot(userID, robotID)
}

// GetRobot function is middleware between GetRobot handler and DB logic
func (s Service) GetRobot(userID, robotID int) (*robots.Robot, error) {
	return s.rs.GetRobot(userID, robotID)
}

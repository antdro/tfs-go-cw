package user

import "time"

// User structure
type User struct {
	ID        int
	Name      string     `json:"name"`
	Email     string     `json:"email"`
	Birthday  *time.Time `json:"birthday"`
	Password  string     `json:"password"`
	CreatedAt *time.Time
	UpdatedAt *time.Time
}

// Storage functions to work with user
type Storage interface {
	InsertUser(*User) error
	SearchUser(string) (int, string, error)
	UpdateUser(User, int) (*User, error)
	SearchUserByID(int) (*User, error)
}
